provider aws {
  region = "${var.aws_region}"
}

resource "aws_key_pair" "myoregonkp" {
  key_name   = "MyOregonKP"
  public_key = "${file("MyOregonKP.pub")}"
}

resource "aws_instance" "myinstance" {
  count = "${var.instance_count}"

  #ami           = "${lookup(var.images,${element(var.aws_region, count.index)}}"
  ami           = "${lookup(var.images,var.aws_region)}"
  instance_type = "${var.instance_type}"

  key_name = "${aws_key_pair.myoregonkp.key_name}"
  #key_name = "MyKP"
  #vpc_security_group_ids = ["sg-0d7f53c6244ab2051"]
  #subnet_id              = "subnet-0b34953152402ad9c"

  tags = {
    #Name  = "Terraform1"
    #Name  = "Terraform-${count.index}"
    #Name  = "Terraform-${count.index + 1}"
    Name = "${element(var.instance_tags, count.index)}"

    Batch = "7AM"
  }
}