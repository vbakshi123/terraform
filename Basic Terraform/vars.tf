variable "instance_type" {
  default = "t2.micro"
}

variable "images" {
  type = "map"

  default = {
    "us-east-1" = "ami-0de53d8956e8dcf80"
    "us-west-2" = "ami-01e24be29428c15b2"
  }
}

variable "aws_region" {
  default = "us-west-2"
}

variable "instance_count" {
  default = "2"
}

variable "instance_tags" {
  type    = "list"
  default = ["Terraform-US-West-1", "Terraform-US-West-2"]
}
