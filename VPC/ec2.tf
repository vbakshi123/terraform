resource "aws_key_pair" "myoregonkp" {
  key_name   = "MyOregonKP"
  public_key = "${file("MyOregonKP.pub")}"
}

resource "aws_instance" "webserver" {
  count = "${var.instance_count}"
  #count = "$length(var.subnet_cidr)}"
  #ami          = "${lookup(var.images,${element(var.aws_region, count.index)}}"
  #availability_zone = "${element(var.subnet_az, count.index)}"
  ami           = "${lookup(var.images,var.aws_region)}"
  instance_type = "${var.instance_type}"
  key_name = "${aws_key_pair.myoregonkp.key_name}"  
  #vpc_security_group_ids = "${aws_security_group.allow_http.id}"
  security_groups = ["${aws_security_group.allow_http.id}"]
  subnet_id      = "${element(aws_subnet.subnet.*.id, count.index)}"
  user_data = "${file("install_httpd.sh")}"

  tags = {
    Name = "${element(var.instance_tags, count.index)}"
    Batch = "7AM"
  }
}