provider aws {
  region = "${var.aws_region}"
}

#Creating new VPC
resource "aws_vpc" "oregonvpc" {
  cidr_block       = "10.0.0.0/16"
  tags = {
    Name = "main"
  }
}

#Creating Internet Gateway and attach it to VPC

resource "aws_internet_gateway" "igw" {
  vpc_id = "${aws_vpc.oregonvpc.id}"
  tags = {
    Name = "InternetGateway"
  }
}

#Creating subnet ID and attach to VPC

resource "aws_subnet" "subnet" {
  count = "${var.count}"
  vpc_id     = "${aws_vpc.oregonvpc.id}"
  cidr_block = "${element(var.subnet_cidr, count.index)}"
  availability_zone = "${element(var.subnet_az, count.index)}"
  map_public_ip_on_launch = "true"
  tags = {
    Name = "Subnet-${count.index + 1}"
  }
}

#Creating Route Table and attach to VPC

resource "aws_route_table" "mainrtb" {
  vpc_id = "${aws_vpc.oregonvpc.id}"
  route {
    cidr_block = "0.0.0.0/0"
    gateway_id = "${aws_internet_gateway.igw.id}"
    }
  }

#Creating Route Table association with subnets
  
resource "aws_route_table_association" "mainrtbassoc" {
  count = "${var.count}"
  subnet_id      = "${element(aws_subnet.subnet.*.id, count.index)}"
  route_table_id = "${aws_route_table.mainrtb.id}"
}

#Create Security Group and attach to VPC

resource "aws_security_group" "allow_http" {
  name        = "allow_http"
  description = "Allow HTTP inbound traffic"
  vpc_id      = "${aws_vpc.oregonvpc.id}"
  ingress {
    from_port   = 80
    to_port     = 80
    protocol    = "tcp"    
    cidr_blocks = ["0.0.0.0/0"]
  }
  
  ingress {
    from_port   = 22
    to_port     = 22
    protocol    = "tcp"    
    cidr_blocks = ["0.0.0.0/0"]
  }
  
  egress {
    from_port       = 0
    to_port         = 0
    protocol        = "-1"
    cidr_blocks     = ["0.0.0.0/0"]    
  }
  tags = {
    Name = "Allow_http"
  }
}
