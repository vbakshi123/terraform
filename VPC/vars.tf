variable "instance_type" {
  default = "t2.micro"
}

variable "images" {
  type = "map"

  default = {
    "us-east-1" = "ami-0de53d8956e8dcf80"
    "us-west-2" = "ami-01e24be29428c15b2"
  }
}

variable "aws_region" {
  default = "us-west-2"
}

variable "instance_count" {
  default = "2"
}

variable "count" {
  default = "2"
}

variable "instance_tags" {
  type    = "list"
  default = ["Terraform-US-West-2a", "Terraform-US-West-2b"]
}


variable "subnet_az" {
  type = "list"
  default = ["us-west-2a", "us-west-2b"]
}

variable "subnet_cidr" {
  type = "list"
  default = ["10.0.1.0/24", "10.0.2.0/24"]
}

